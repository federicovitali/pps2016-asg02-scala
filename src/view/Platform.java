package view;

import game.Background;
import game.Keyboard;
import controller.LevelController;
import gameElements.activeElements.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;

import gameElements.activeElements.Character;
import gameElements.staticElements.*;
import utils.Res;

public class Platform extends JPanel {

    private static final int MIN_LEVEL_X = 0;
    private static final int MAX_LEVEL_X = 4600;
    private static final int MIN_BACKGROUND_X = -800;
    private static final int MAX_BACKGROUND_X = 800;
    private static final int START_FIRST_BACKGROUND = -50;
    private static final int START_SECOND_BACKGROUND = 750;

    private int movement;
    private int xPosition;
    private int floorOffsetY;
    private int heightLimit;
    private Mario mario;
    private Background firstBackground;
    private Background secondBackground;
    private List<Obstacle> objects = new ArrayList<>();
    private List<Obstacle> coins = new ArrayList<>();
    private List<Character> enemies = new ArrayList<>();
    private LevelController levelController;
    private JLabel coinsLabel;
    private JLabel infoLabel;
    private int coinsInitialSize;
    private boolean running = true;

    public Platform() {
        super();
        this.movement = 0;
        this.xPosition = -1;
        this.floorOffsetY = 293;
        this.heightLimit = 0;
        this.firstBackground = new Background(START_FIRST_BACKGROUND);
        this.secondBackground = new Background(START_SECOND_BACKGROUND);
        this.setFocusable(true);
        this.requestFocusInWindow();
        this.addKeyListener(new Keyboard(this));
        this.coinsLabel  = new JLabel("Coins: ");
        this.coinsLabel.setFont(new Font(coinsLabel.getName(),Font.BOLD,20));
        setLayout(new FlowLayout(FlowLayout.LEFT));
        add(coinsLabel);
        this.infoLabel  = new JLabel("Take all the coins to complete the level!");
        this.infoLabel.setFont(new Font(infoLabel.getName(),Font.BOLD,20));
        setLayout(new FlowLayout(FlowLayout.CENTER));
        add(infoLabel);
    }

    public int getFloorOffsetY() {
        return floorOffsetY;
    }

    public int getHeightLimit() {
        return heightLimit;
    }

    public int getMovement() {
        return movement;
    }

    public int getXPosition() {
        return xPosition;
    }

    public void setFloorOffsetY(int floorOffsetY) {
        this.floorOffsetY = floorOffsetY;
    }

    public void setHeightLimit(int heightLimit) {
        this.heightLimit = heightLimit;
    }

    public void setXPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public void setMovement(int movement) {
        this.movement = movement;
    }

    public Mario getMario(){
        return this.mario;
    }

    private void movingScreen(){
        if (this.xPosition >= MIN_LEVEL_X && this.xPosition <= MAX_LEVEL_X) {
            this.xPosition = this.xPosition + this.movement;
            this.firstBackground.background_$eq(this.firstBackground.background()-this.movement);
            this.secondBackground.background_$eq(this.secondBackground.background()-this.movement);
        }
    }

    public void setObjects(LevelController itemsInTheLevel){
        this.levelController = itemsInTheLevel;
        this.mario = (Mario) levelController.getMario();
        this.coins = levelController.getCoinsElements();
        this.coinsInitialSize = coins.size();
        this.coinsLabel.setText("Coins: 0/"+ coinsInitialSize);
        this.objects = levelController.getStaticElements();
        this.enemies = levelController.getActiveElements();
    }

    private void changingBackground(){
        if (this.firstBackground.background() == MIN_BACKGROUND_X) {
            this.firstBackground.background_$eq(MAX_BACKGROUND_X);
        }else if (this.secondBackground.background() ==MIN_BACKGROUND_X) {
            this.secondBackground.background_$eq(MAX_BACKGROUND_X);
        }else if (this.firstBackground.background() == MAX_BACKGROUND_X) {
            this.firstBackground.background_$eq(MIN_BACKGROUND_X);
        }else if (this.secondBackground.background() == MAX_BACKGROUND_X) {
            this.secondBackground.background_$eq(MIN_BACKGROUND_X);
        }
    }

    private void updateBackgroundOnMovement() {
        movingScreen();
        changingBackground();
    }

    public void paintComponent(Graphics g) {
        if(running) {
            this.levelController.doAllTheChecks();
            this.updateBackgroundOnMovement();
            this.drawAllCharactersAndObjects(g);
            this.drawMarioIfJumping(g);
            this.drawEnemiesIfAliveOrNot(g);
        }
    }

    private void drawAllCharactersAndObjects(Graphics g){
        g.drawImage(firstBackground.imageBackground(), firstBackground.background(), MIN_LEVEL_X, null);
        g.drawImage(secondBackground.imageBackground(), secondBackground.background(), MIN_LEVEL_X, null);

        for (Obstacle object : objects) {
            g.drawImage(object.imgObj(), object.position().X(),
                    object.position().Y(), null);
        }
        this.coinsLabel.setText("Coins: "+ (coinsInitialSize -coins.size()) +"/"+ coinsInitialSize);
        for (Obstacle coin : coins) {
            g.drawImage(coin.imgObj(), coin.position().X(),
                    coin.position().Y(), null);
        }
    }

    private void drawMarioIfJumping(Graphics g){
        if (this.mario.jumping())
            g.drawImage(this.mario.doJump(), this.mario.position().X(), this.mario.position().Y(), null);
        else
            g.drawImage(this.mario.walk(Res.IMGP_CHARACTER_MARIO, this.mario.characterFrequency()), this.mario.position().X(), this.mario.position().Y(), null);
    }

    private void drawEnemiesIfAliveOrNot(Graphics g){
        for(Character enemy: enemies) {
            if (enemy.alive())
                g.drawImage(enemy.walk(enemy.resImageAlive(), enemy.characterFrequency()), enemy.position().X(),enemy.position().Y(), null);
            else {
                g.drawImage(enemy.deadImage(), enemy.position().X(), enemy.position().Y() + enemy.characterOffset(), null);
            }
        }
    }

    public void winTheLevel(){
        this.running = false;
    }
}
