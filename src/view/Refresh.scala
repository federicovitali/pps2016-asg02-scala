package view

object Refresh {
  private val PAUSE = 4
}

class Refresh(var level: Platform) extends Runnable {
  private var isRunning:Boolean = true
  override def run(): Unit = while (isRunning) {level.repaint();try Thread.sleep(Refresh.PAUSE) catch {case e: InterruptedException => e.printStackTrace()}}
  def stopIt():Unit = isRunning = false
}