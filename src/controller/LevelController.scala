package controller

import game.{Audio, Main}
import gameElements.AbstractGameElementsFactory
import view.Platform
import gameElements.activeElements.Mario
import gameElements.staticElements._
import gameElements.activeElements.Character
import utils.Position
import utils.Res
import utils.Utils

import scala.collection.JavaConverters._
import scala.collection.mutable.ArrayBuffer

object LevelController {
  private val TYPE_WITH_SPACING = 1
  private val TYPE_WITH_NO_SPACING = 0
  private val MIN_LEVEL_X = 0
  private val MAX_LEVEL_X = 4600
}

class LevelController(var gameFactory: AbstractGameElementsFactory, var numbersOfTunnel: Int, var numbersOfBlock: Int, var numbersOfPiece: Int, var numbersOfEnemies: Int, var level: Platform) {
  private val passiveObjects = ArrayBuffer[Obstacle]()
  private var coinsInTheLevel = ArrayBuffer[Obstacle]()
  private val enemies = ArrayBuffer[Character]()
  private val mainCharacter = gameFactory.createMainCharacter(new Position(300, 245), level)
  createElements()

  private def createElements() = {
    1 to numbersOfBlock foreach {_ => passiveObjects += gameFactory.createBlock(new Position(Utils.getRandomCoordinate(4600, 400, LevelController.TYPE_WITH_SPACING), Utils.getRandomCoordinate(210, 160, LevelController.TYPE_WITH_NO_SPACING)), level)}
    1 to numbersOfPiece foreach {_ => coinsInTheLevel += gameFactory.createCoin(new Position(Utils.getRandomCoordinate(4600, 400, LevelController.TYPE_WITH_SPACING), Utils.getRandomCoordinate(145, 45, LevelController.TYPE_WITH_NO_SPACING)), level)}
    1 to numbersOfTunnel foreach {_=> passiveObjects += gameFactory.createTunnel(new Position(Utils.getRandomCoordinate(4600, 400, LevelController.TYPE_WITH_SPACING), 230), level)}
    1 to numbersOfEnemies foreach {_=> enemies += gameFactory.createMushroom(new Position(Utils.getRandomCoordinate(4600, 400, LevelController.TYPE_WITH_SPACING), 263), level); enemies+=gameFactory.createTurtle(new Position(Utils.getRandomCoordinate(4600, 400, LevelController.TYPE_WITH_SPACING), 243), level)}
    passiveObjects += gameFactory.createCastle(new Position(10, 95), level);passiveObjects+= gameFactory.createCastle(new Position(4850, 95), level)
    passiveObjects += gameFactory.createPlaque(new Position(220, 234), level);passiveObjects+=gameFactory.createFlag(new Position(4650, 115), level)
  }

  def getActiveElements:java.util.List[Character] = enemies.asJava
  def getMario: Character = mainCharacter
  def getCoinsElements: java.util.List[Obstacle] = coinsInTheLevel.asJava
  def getStaticElements: java.util.List[Obstacle] = passiveObjects.asJava

  def doAllTheChecks(): Unit = {
    checkContactWithObjects()
    checkMarioPickMoney()
    checkContactBetweenCharacters()
    moveObjectsAndCharacters()
  }

  private def checkContactWithObjects(): Unit = {
    for (objectsInGame <- passiveObjects) {if (mainCharacter.isNearby(objectsInGame)) {mainCharacter.contact(objectsInGame)
      if(objectsInGame.imgObj.equals(Utils.getImage(Res.IMG_CASTLE)) && coinsInTheLevel.isEmpty){Main.stopTimer()}}
      for (enemy <- enemies) {if (enemy.isNearby(objectsInGame)) enemy.contact(objectsInGame)}}
  }

  private def checkContactBetweenCharacters() = {
    for(i <- enemies.indices) {
      for (j <- i + 1 until enemies.size) {if (enemies(i).isNearby(enemies(j))) enemies(i).contact(enemies(j))
        if (mainCharacter.isNearby(enemies(i))) mainCharacter.contact(enemies(i))}
    }
  }

  private def checkMarioPickMoney():Unit = {for (coin <- coinsInTheLevel){if (mainCharacter.asInstanceOf[Mario].contactPiece(coin)) {Audio.playSound(Res.AUDIO_MONEY);coinsInTheLevel-=coin;return}}}

  private def moveObjectsAndCharacters() = if (level.getXPosition >= LevelController.MIN_LEVEL_X && level.getXPosition <= LevelController.MAX_LEVEL_X) {
    for (objectsInGame <- passiveObjects)objectsInGame.move()
    for (coin <- coinsInTheLevel)coin.move()
    for (character <- enemies)character.move()
  }
}