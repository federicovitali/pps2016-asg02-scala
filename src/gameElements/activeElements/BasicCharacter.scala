package gameElements.activeElements

import java.awt.Image
import gameElements.GameElement
import view.Platform
import utils.Position
import utils.Res
import utils.Utils

object BasicCharacter {
  private val PROXIMITY_MARGIN = 10
}

abstract class BasicCharacter(override val position: Position, var width: Int, var height: Int, override val level: Platform) extends Character{
  private var counter:Int = 0
  private var _moving:Boolean = false
  private var _toRight:Boolean = true
  private var _alive:Boolean = true
  private var _offset:Int = 1

  override def moving: Boolean = _moving
  override def moving_=(state: Boolean): Unit = _moving = state
  override def right: Boolean = _toRight
  override def right_=(state: Boolean): Unit = _toRight = state
  override def alive: Boolean = _alive
  override def alive_=(state: Boolean): Unit = _alive = state
  override def offsetOnMovement: Int = _offset
  override def offsetOnMovement_=(state: Int): Unit = _offset = state

  override def walk(name: String, frequency: Int): Image = {val str = Res.IMG_BASE + name +
    (if ( {this.counter += 1; this.counter} % frequency == 0) Res.IMGP_STATUS_ACTIVE else Res.IMGP_STATUS_NORMAL)+
    (if (right) Res.IMGP_DIRECTION_DX else Res.IMGP_DIRECTION_SX) +
    Res.IMG_EXT; Utils.getImage(str)}

  override def move(): Unit = {offsetOnMovement = if (right) 1 else -1; position.X = position.X + offsetOnMovement}

  override def hitAbove(`object`: GameElement): Boolean = !(position.X + width < `object`.position.X + 5 || position.X > `object`.position.X + `object`.width - 5 ||
    position.Y < `object`.position.Y + `object`.height || position.Y > `object`.position.Y + `object`.height + 5)

  override def hitAhead(character: GameElement): Boolean = right && !(position.X + width < character.position.X || position.X + width > character.position.X + 5 ||
    position.Y + height <= character.position.Y || position.Y >= character.position.Y + character.height)

  override def hitBack(character: GameElement): Boolean = !(position.X > character.position.X + character.width || position.X + width < character.position.X + character.width - 5 ||
    position.Y + height <= character.position.Y || position.Y >= character.position.Y + character.height)

  override def hitBelow(character: GameElement): Boolean = !(position.X + width < character.position.X || position.X > character.position.X + character.width ||
    position.Y + height < character.position.Y || position.Y + height > character.position.Y)

  override def isNearby(`object`: GameElement): Boolean = (position.X > `object`.position.X - BasicCharacter.PROXIMITY_MARGIN && position.X < `object`.position.X + `object`.width + BasicCharacter.PROXIMITY_MARGIN) ||
    (position.X + width > `object`.position.X - BasicCharacter.PROXIMITY_MARGIN && position.X + width < `object`.position.X + `object`.width + BasicCharacter.PROXIMITY_MARGIN)

  override def contact[T](obj: T): Unit = if (this.hitAhead(obj.asInstanceOf[GameElement]) && right) { right = false; offsetOnMovement = -1}
  else if (this.hitBack(obj.asInstanceOf[GameElement]) && !right) {right = true ; offsetOnMovement = 1}

}