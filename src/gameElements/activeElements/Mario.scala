package gameElements.activeElements

import java.awt.Image
import gameElements.staticElements.Obstacle
import view.Platform
import utils.Position
import utils.Res
import utils.Utils

object Mario {
  private val MARIO_FREQUENCY = 100
  private val MARIO_OFFSET_Y_INITIAL = 243
  private val FLOOR_OFFSET_Y_INITIAL = 293
  private val WIDTH = 28
  private val HEIGHT = 50
  private val JUMPING_LIMIT = 42
  private val DOWN_OFFSET_Y = 4
}

class Mario(override val position: Position,override val level: Platform) extends BasicCharacter(position, Mario.WIDTH, Mario.HEIGHT, level) {

  private var jumpingExtent = 0
  private var _jumping = false

  def jumping:Boolean = _jumping

  def jumping_=(value:Boolean):Unit = _jumping = value

  def doJump(): Image = {
    var resImage:String = null; this.jumpingExtent += 1
    if (this.jumpingExtent < Mario.JUMPING_LIMIT) {
      if (position.Y > level.getHeightLimit) position.Y_$eq(position.Y - Mario.DOWN_OFFSET_Y)
      else this.jumpingExtent = Mario.JUMPING_LIMIT
      resImage = if (right) Res.IMG_MARIO_SUPER_DX else Res.IMG_MARIO_SUPER_SX
    }
    else if (position.Y + this.height < level.getFloorOffsetY) {position.Y_$eq(position.Y + 1); resImage = if (right) Res.IMG_MARIO_SUPER_DX else Res.IMG_MARIO_SUPER_SX}
    else { resImage = if (right) Res.IMG_MARIO_ACTIVE_DX else Res.IMG_MARIO_ACTIVE_SX; jumping = false; this.jumpingExtent = 0}
    Utils.getImage(resImage)
  }

  def contactPiece(coin: Obstacle): Boolean = this.hitBack(coin) || this.hitAbove(coin) || this.hitAhead(coin) || this.hitBelow(coin)

  override def deadImage = null

  override def resImageAlive = Res.IMG_MARIO_DEFAULT

  override def characterFrequency: Int = Mario.MARIO_FREQUENCY

  override def characterOffset: Int = Mario.MARIO_OFFSET_Y_INITIAL

  override def contact[T](`object`: T): Unit = `object` match {
    case _:Obstacle => contactObstacle(`object`.asInstanceOf[Obstacle])
    case _:BasicCharacter => contactCharacter(`object`.asInstanceOf[BasicCharacter])
  }

  private def contactCharacter(character: BasicCharacter) = if (this.hitAhead(character) || this.hitBack(character)){if (character.alive) {moving = false;alive = false}}
  else if (this.hitBelow(character)) {character.moving =false;character.alive = false}

  private def contactObstacle(`object`: Obstacle) = {
    if (this.hitAhead(`object`) && right || this.hitBack(`object`) && !right) {level.setMovement(0);moving = false}
    if (this.hitBelow(`object`) && this.jumping) level.setFloorOffsetY(`object`.position.Y)
    else if (!this.hitBelow(`object`)) {level.setFloorOffsetY(Mario.FLOOR_OFFSET_Y_INITIAL)
      if (!this.jumping) position.Y_$eq(Mario.MARIO_OFFSET_Y_INITIAL)
      if (hitAbove(`object`)) level.setHeightLimit(`object`.position.Y + `object`.height)
      else if (!this.hitAbove(`object`) && !this.jumping) level.setHeightLimit(0)}
  }
}