package gameElements.activeElements

import gameElements.GameElement
import java.awt.Image

import view.Platform

trait Character extends GameElement {

  def walk(name: String, frequency: Int): Image
  def isNearby(`object`: GameElement): Boolean
  def hitBelow(character: GameElement): Boolean
  override def move(): Unit
  def level: Platform
  def hitAbove(`object`: GameElement): Boolean
  def hitAhead(character: GameElement): Boolean
  def hitBack(character: GameElement): Boolean
  def contact[T](`object`: T): Unit
  def alive: Boolean
  def alive_=(state : Boolean ): Unit
  def moving:Boolean
  def moving_=(state : Boolean ): Unit
  def right:Boolean
  def right_=(state : Boolean ): Unit
  def deadImage: Image
  def resImageAlive: String
  def characterFrequency: Int
  def characterOffset: Int
  def offsetOnMovement: Int
  def offsetOnMovement_=(state : Int ): Unit

}