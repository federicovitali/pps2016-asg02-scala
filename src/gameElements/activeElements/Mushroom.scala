package gameElements.activeElements

import java.awt.Image
import gameElements.GameElement
import view.Platform
import utils.Position
import utils.Res
import utils.Utils

object Mushroom {
  private val WIDTH = 27
  private val HEIGHT = 30
  private val PAUSE = 15
  private val MUSHROOM_FREQUENCY = 10
  private val MUSHROOM_DEAD_OFFSET_Y = 20
}

class Mushroom(override val position: Position, override val level: Platform) extends BasicCharacter(position, Mushroom.WIDTH, Mushroom.HEIGHT, level) with Runnable {
  right = true
  moving = true
  val chrMushroom: Unit = new Thread(this).start()

  override def characterFrequency: Int = Mushroom.MUSHROOM_FREQUENCY

  override def characterOffset: Int = Mushroom.MUSHROOM_DEAD_OFFSET_Y

  override def run(): Unit = while(true) if (alive) {move(); try Thread.sleep(Mushroom.PAUSE) catch {case e: InterruptedException => e.printStackTrace()}}

  override def deadImage: Image = Utils.getImage(if(right) Res.IMG_MUSHROOM_DEAD_DX else Res.IMG_MUSHROOM_DEAD_SX)

  override def resImageAlive: String = Res.IMGP_CHARACTER_MUSHROOM

}