package gameElements.activeElements

import java.awt.Image
import view.Platform
import utils.Position
import utils.Res
import utils.Utils

object Turtle {
  private val WIDTH = 43
  private val HEIGHT = 50
  private val PAUSE = 15
  private val TURTLE_FREQUENCY = 10
  private val TURTLE_DEAD_OFFSET_Y = 30
}

class Turtle(override val position: Position,override val level: Platform) extends BasicCharacter(position, Turtle.WIDTH, Turtle.HEIGHT, level) with Runnable {
  right = true
  moving = true
  val chrTurtle: Unit = new Thread(this).start()

  override def characterFrequency: Int = Turtle.TURTLE_FREQUENCY

  override def characterOffset: Int = Turtle.TURTLE_DEAD_OFFSET_Y

  override def run(): Unit = while(true) if (alive) {move(); try Thread.sleep(Turtle.PAUSE) catch {case e: InterruptedException => e.printStackTrace()}}

  override def deadImage: Image = Utils.getImage(Res.IMG_TURTLE_DEAD)

  override def resImageAlive: String = Res.IMGP_CHARACTER_TURTLE

}