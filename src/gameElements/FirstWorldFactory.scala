package gameElements

import view.Platform
import gameElements.activeElements.Mario
import gameElements.activeElements.Mushroom
import gameElements.activeElements.Turtle
import gameElements.staticElements._
import gameElements.activeElements.Character
import utils.Position

/**
  * Created by Federico on 16/03/2017.
  */
class FirstWorldFactory extends AbstractGameElementsFactory {
  override def createMainCharacter(position: Position, level: Platform) = new Mario(position, level)

  override def createMushroom(position: Position, level: Platform) = new Mushroom(position, level)

  override def createTurtle(position: Position, level: Platform) = new Turtle(position, level)

  override def createTunnel(position: Position, level: Platform):Obstacle = Obstacle("Tunnel",position, level)

  override def createCoin(position: Position, level: Platform):Obstacle = Obstacle("Coin",position, level)

  override def createBlock(position: Position, level: Platform):Obstacle = Obstacle("Block",position, level)

  override def createFlag(position: Position, level: Platform):Obstacle = Obstacle("Flag",position, level)

  override def createCastle(position: Position, level: Platform):Obstacle = Obstacle("Castle",position, level)

  override def createPlaque(position: Position, level: Platform):Obstacle = Obstacle("StartPlaque",position, level)
}