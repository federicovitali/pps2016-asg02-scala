package gameElements

import view.Platform
import gameElements.staticElements._
import utils.Position
import gameElements.activeElements.Character

/**
  * Created by Federico on 16/03/2017.
  */
trait AbstractGameElementsFactory {
  def createMainCharacter(position: Position, level: Platform): Character

  def createMushroom(position: Position, level: Platform): Character

  def createTurtle(position: Position, level: Platform): Character

  def createTunnel(position: Position, level: Platform): Obstacle

  def createCoin(position: Position, level: Platform): Obstacle

  def createBlock(position: Position, level: Platform): Obstacle

  def createFlag(position: Position, level: Platform): Obstacle

  def createCastle(position: Position, level: Platform): Obstacle

  def createPlaque(position: Position, level: Platform): Obstacle
}