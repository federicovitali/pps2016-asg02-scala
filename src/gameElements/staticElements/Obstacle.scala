package gameElements.staticElements

import gameElements.GameElement
import java.awt._

import utils.{Position, Res, Utils}
import view.Platform

/*trait Obstacle extends GameElement {
  def getImgObj: Image
}*/

trait Obstacle extends GameElement {
  def level: Platform
  def imgObj: Image
  def move(): Unit = position.X = position.X - level.getMovement
}

object ObstacleOnMovement {
  private val MAX_Y = 230
  private val MIN_Y = 120
}

trait ObstacleOnMovement extends Obstacle {
  private var increment:Boolean = false
  override def move(): Unit = {if(increment)position.Y += 1 else position.Y -= 1
    if(position.Y==ObstacleOnMovement.MIN_Y)increment = true else if(position.Y==ObstacleOnMovement.MAX_Y)increment = false; super.move()}
}

object Obstacle {
  def apply(s: String, gamePosition: Position,level: Platform) :Obstacle = s match {
    case "Block" => new Block(gamePosition,level)
    case "MovementBlock" => new BlockOnMovement(gamePosition,level)
    case "Castle" => new Castle(gamePosition,level)
    case "Coin" => new Coin(gamePosition,level)
    case "Flag" => new Flag(gamePosition,level)
    case "StartPlaque" => new StartPlaque(gamePosition,level)
    case "Tunnel" => new Tunnel(gamePosition,level)
  }

  private val WIDTH_LITTLE_OBJECT = 30
  private val HEIGHT_LITTLE_OBJECT = 30
  private val WIDTH_NORMAL_OBJECT = 43
  private val HEIGHT_NORMAL_OBJECT = 65


  private class Block(override val position: Position, override val level: Platform)extends Obstacle {
    override def width: Int = Obstacle.WIDTH_LITTLE_OBJECT
    override def height: Int = Obstacle.HEIGHT_LITTLE_OBJECT
    override def imgObj: Image = Utils.getImage(Res.IMG_BLOCK)
  }

  private class BlockOnMovement(override val position: Position, override val level: Platform)extends Obstacle with  ObstacleOnMovement{
    override def width: Int = Obstacle.WIDTH_LITTLE_OBJECT
    override def height: Int = Obstacle.HEIGHT_LITTLE_OBJECT
    override def imgObj: Image = Utils.getImage(Res.IMG_BLOCK)
  }

  private class Castle(override val position: Position, override val level: Platform)extends Obstacle{
    override def width: Int = Obstacle.WIDTH_NORMAL_OBJECT
    override def height: Int = Obstacle.HEIGHT_NORMAL_OBJECT
    override def imgObj: Image = Utils.getImage(Res.IMG_CASTLE)
  }

  private class Flag(override val position: Position, override val level: Platform)extends Obstacle{
    override def width: Int = Obstacle.WIDTH_NORMAL_OBJECT
    override def height: Int = Obstacle.HEIGHT_NORMAL_OBJECT
    override def imgObj: Image = Utils.getImage(Res.IMG_FLAG)
  }

  private class StartPlaque(override val position: Position, override val level: Platform)extends Obstacle{
    override def width: Int = Obstacle.WIDTH_NORMAL_OBJECT
    override def height: Int = Obstacle.HEIGHT_NORMAL_OBJECT
    override def imgObj: Image = Utils.getImage(Res.START_ICON)
  }

  private class Tunnel(override val position: Position, override val level: Platform)extends Obstacle{
    override def width: Int = Obstacle.WIDTH_NORMAL_OBJECT
    override def height: Int = Obstacle.HEIGHT_NORMAL_OBJECT
    override def imgObj: Image = Utils.getImage(Res.IMG_TUNNEL)
  }

  object Coin {
    private val PAUSE = 10
    private val FLIP_FREQUENCY = 150
  }

  private class Coin(override val position: Position, override val level: Platform)extends Obstacle with Runnable{
    override def width: Int = Obstacle.WIDTH_LITTLE_OBJECT
    override def height: Int = Obstacle.HEIGHT_LITTLE_OBJECT
    override def imgObj: Image = Utils.getImage(Res.IMG_PIECE1)
    private var counter = 0

    def imageOnMovement: Image = Utils.getImage(if ( { this.counter += 1; this.counter} % Coin.FLIP_FREQUENCY == 0) Res.IMG_PIECE1 else Res.IMG_PIECE2)

    override def run(): Unit = while (true) { imageOnMovement; try Thread.sleep(Coin.PAUSE) catch {case e: InterruptedException => e.printStackTrace()}}
  }
}