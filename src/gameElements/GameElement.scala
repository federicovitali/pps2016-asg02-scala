package gameElements

import utils.Position

/**
  * Created by Federico on 12/03/2017.
  */
/*trait GameElement {
  def getWidth: Int

  def getHeight: Int

  def getPosition: Position

  def move(): Unit
}*/
trait GameElement {
  def width: Int

  def height: Int

  def position: Position

  def move(): Unit
}