package game

import utils.Res
import utils.Utils
import java.awt._

/**
  * Created by Federico on 12/03/2017.
  */
class Background(private var _positionX: Int) {

  def imageBackground:Image = Utils.getImage(Res.IMG_BACKGROUND)
  def background_=(x: Int): Unit = _positionX = x
  def background: Int = _positionX
}