package game;

import gameElements.AbstractGameElementsFactory;
import gameElements.FirstWorldFactory;
import controller.LevelController;
import gameElements.SecondWorldFactory;
import view.Platform;
import view.Refresh;

import javax.swing.*;
import java.awt.*;

public class Main {

    private static final int WINDOW_WIDTH = 700;
    private static final int WINDOW_HEIGHT = 360;
    private static final int NUMBERS_OF_TUNNEL = 10;
    private static final int NUMBERS_OF_BLOCK = 15;
    private static final int NUMBERS_OF_PIECE = 7;
    private static final int NUMBERS_OF_ENEMIES = 2;
    private static final String WINDOW_TITLE = "Super Mario";
    private static AbstractGameElementsFactory firstLevelFactory = new FirstWorldFactory();
    private static AbstractGameElementsFactory  secondLevelFactory= new SecondWorldFactory();
    private static Platform scene;
    private static JFrame panel;
    private static Refresh timer;

    public static void main(String[] args) {
        panel = new JFrame(WINDOW_TITLE);
        panel.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        panel.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        panel.setLocationRelativeTo(null);
        panel.setResizable(false);
        panel.setAlwaysOnTop(true);
        scene = new Platform();
        LevelController objectsInTheLevel = new LevelController(firstLevelFactory/*secondLevelFactory*/,NUMBERS_OF_TUNNEL,NUMBERS_OF_BLOCK,NUMBERS_OF_PIECE,NUMBERS_OF_ENEMIES,scene);
        scene.setObjects(objectsInTheLevel);
        panel.setContentPane(scene);
        panel.setVisible(true);

        timer = new Refresh(scene);
        new Thread(timer).start();
    }

    public static void stopTimer(){
        scene.winTheLevel();
        timer.stopIt();
        Object[] options = {"New Level!","Close Game!"};
        int value = JOptionPane.showOptionDialog(panel,"Level Completed! What you want to do?","SELECT",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE,null,options,options[0]);
        if(value == JOptionPane.NO_OPTION)
            System.exit(0);
        if(value == JOptionPane.YES_OPTION) {
            System.exit(0); //TO DO NEW LEVELS
        }

    }



}
