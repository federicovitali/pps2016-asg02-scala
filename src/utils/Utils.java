package utils;

import javax.swing.*;
import java.awt.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;

/**
 * @author Roberto Casadei
 */

public class Utils {

    private static final int MAX_SPACE = 50;

    private static ArrayList<Integer> numbersAlreadyExtracted = new ArrayList<>();

    public static URL getResource(String path){
        return Utils.class.getClass().getResource(path);
    }

    public static Image getImage(String path){
        return new ImageIcon(getResource(path)).getImage();
    }

    public static int getRandomCoordinate(int high, int low, int type){
        int randomNumber =0;
        switch (type) {
            case 1:
                do {
                    randomNumber = new Random().nextInt(high - low) + low;
                } while (numbersAlreadyExtracted.contains(randomNumber) || spaceObjects(randomNumber));
                numbersAlreadyExtracted.add(randomNumber);
            case 0:
                do {
                    randomNumber = new Random().nextInt(high - low) + low;
                } while (numbersAlreadyExtracted.contains(randomNumber));
                numbersAlreadyExtracted.add(randomNumber);
        }
        return randomNumber;
    }

     private static boolean spaceObjects(int numberExtracted) {
         for (int j = 0; j < MAX_SPACE; j++) {
             if (numbersAlreadyExtracted.contains(numberExtracted + j) || numbersAlreadyExtracted.contains(numberExtracted - j)) {
                 return true;
             }
         }
         return false;
     }
}
