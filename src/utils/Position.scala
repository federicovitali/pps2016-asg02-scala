package utils

/**
  * Created by Federico on 14/03/2017.
  */
class Position(private var _X: Int,private var _Y: Int) extends Point {

  override def X_=(x: Int): Unit = _X = x

  override def Y_=(y: Int): Unit = _Y = y

  override def X: Int = _X

  override def Y: Int = _Y
}

