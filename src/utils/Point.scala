package utils

/**
  * Created by Federico on 14/03/2017.
  */
trait Point {
  def X: Int
  def X_=(x: Int): Unit
  def Y: Int
  def Y_=(y: Int): Unit

}